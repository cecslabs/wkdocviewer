/*
 * wkdocviewer - WebKit2-based Document Viewer
 *
 * by Robert (bob) Edwards, March 2022
 *
 * based on "One-Window Browser" tutorial
 * (https://wiki.gnome.org/Projects/WebKitGtk/ProgrammingGuide/Tutorial)
 *
 * run with: DISPLAY=:x wkdocviewer -x 1280 -y 800 -z 1.3 someURL
 *
 * see LICENSE file for license details
 */

#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <webkit2/webkit2.h>
#include <signal.h>
#include <wait.h>

static void destroyWindowCb (GtkWidget* widget, GtkWidget* window) {
	gtk_main_quit ();
}

static gboolean closeWebViewCb (WebKitWebView* webView, GtkWidget* window) {
	gtk_widget_destroy (window);
	return TRUE;
}

WebKitWebView *WebView;		/* global so we can send signals to it... */

void sighup_handler (int unused) {
	webkit_web_view_reload_bypass_cache (WebView);
} /* sighup_handler () */

void sigchld_handler (int unused) {
	if (signal (SIGCHLD, sigchld_handler) == SIG_ERR) {
		fprintf (stderr, "Can't install SIGCHLD handler");
		exit (EXIT_FAILURE);
	}
	while (waitpid (-1, NULL, WNOHANG) > 0);
} /* sigchld_handler () */

int main (int argc, char* argv[]) {
	int verbose = 0;
	int xsize = 1024;
	int ysize = 768;
	double zoom = 1.0;
	char opt;

	gtk_init (&argc, &argv);

	while ((opt = getopt (argc, argv, "vx:y:z:")) != -1) {
		switch (opt) {
			case 'v':
				verbose = 1; break;
			case 'x':
				xsize = atoi (optarg); break;
			case 'y':
				ysize = atoi (optarg); break;
			case 'z':
				zoom = strtod (optarg, NULL); break;
			default: 
				fprintf (stderr, "Usage: %s [-d display] [-v] [-x xsize] [-y ysize] [-z zoom_factor] url\n", argv[0]);
				exit (EXIT_FAILURE);
		}
	}

	if (verbose)
		printf ("xsize: %d, ysize %d, zoom %f, url: '%s'\n", xsize, ysize, zoom, argv[optind]);

	sigchld_handler (0);
	if (signal (SIGHUP, sighup_handler) == SIG_ERR) {
		fprintf (stderr, "Can't install SIGHUP handler");
		exit (EXIT_FAILURE);
	}

	GtkWidget *Window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (GTK_WINDOW (Window), xsize, ysize);

	WebView = WEBKIT_WEB_VIEW (webkit_web_view_new ());
	gtk_container_add (GTK_CONTAINER (Window), GTK_WIDGET (WebView));

	g_signal_connect (Window, "destroy", G_CALLBACK (destroyWindowCb), NULL);
	g_signal_connect (WebView, "close", G_CALLBACK (closeWebViewCb), Window);

	WebKitWebContext *Ctx = webkit_web_view_get_context (WebView);
	webkit_web_context_set_cache_model (Ctx, WEBKIT_CACHE_MODEL_DOCUMENT_VIEWER);

	webkit_web_view_set_zoom_level (WebView, zoom);
	webkit_web_view_load_uri (WebView, argv[optind]);

	gtk_widget_show_all (Window);
	gtk_main ();

	return EXIT_SUCCESS;
} /* main () */
