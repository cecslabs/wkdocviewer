# wkdocviewer - simple HTML document viewer
# See LICENSE file for copyright and license details.
.POSIX:

VERSION = 1.0

PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man
CFLAGS = -Wall
GTKINC = `pkg-config --cflags gtk+-3.0 webkit2gtk-4.0`
INCS = $(GTKINC)
GTKLIB = `pkg-config --libs gtk+-3.0 webkit2gtk-4.0`
LIBS = $(GTKLIB) -lgthread-2.0

all: options wkdocviewer

options:
	@echo wkdocviewer build options:
	@echo "CC            = $(CC)"
	@echo "CFLAGS        = $(CFLAGS)"
	@echo "LDFLAGS       = $(LDFLAGS)"

wkdocviewer: wkdocviewer.c
	$(CC) $(INCS) $(LDFLAGS) -o $@ wkdocviewer.c $(LIBS)

clean:
	rm -f wkdocviewer

distclean: clean
	rm -f wkdocviewer-$(VERSION).tar.gz

dist: distclean
	mkdir -p wkdocviewer-$(VERSION)
	cp -R LICENSE Makefile Readme.md \
	    wkdocviewer.c wkdocviewer-$(VERSION)
	tar -czf wkdocviewer-$(VERSION).tar.gz wkdocviewer-$(VERSION)
	rm -rf wkdocviewer-$(VERSION)

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f wkdocviewer $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/wkdocviewer
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	sed "s/VERSION/$(VERSION)/g" < wkdocviewer.1 > $(DESTDIR)$(MANPREFIX)/man1/wkdocviewer.1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/wkdocviewer.1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/wkdocviewer
	rm -f $(DESTDIR)$(MANPREFIX)/man1/wkdocviewer.1

.SUFFIXES: .so .o .c
.PHONY: all options clean dist install uninstall
