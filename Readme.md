# wkdocviewer
A WebKit2GTK-based HTML/Javascript document viewer intended to be run in an Info Display environment, with no Window Manager, mouse or keyboard.

Based on the "One-Window Browser" WebKit2GTK example at: 
    https://wiki.gnome.org/Projects/WebKitGtk/ProgrammingGuide/Tutorial

## Building
On a Debian/Ubuntu style system, install all the build dependencies for the surf browser:
    apt build-dep surf

Then:
    make

## Installing
On a Debian/Ubuntu install all dependencies for surf browser:
    sudo apt-get install $(apt-cache depends surf | grep Depends | sed -e "s/.*ends:\ //" -e 's/<[^>]*>//' | tr '\n' ' ')

## Running
    DISPLAY=:0 wkdocviewer -x 1280 -y960 -z1.2 <some URL> &

